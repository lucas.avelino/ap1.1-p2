﻿namespace reservationSalles2018
{
    partial class frmM2LReservationSalles
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.sallesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.liguesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.réservantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.equipementsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.réservationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disponibilitésToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ouiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sallesToolStripMenuItem,
            this.liguesToolStripMenuItem,
            this.réservantsToolStripMenuItem,
            this.equipementsToolStripMenuItem,
            this.réservationsToolStripMenuItem,
            this.disponibilitésToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1094, 33);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // sallesToolStripMenuItem
            // 
            this.sallesToolStripMenuItem.Name = "sallesToolStripMenuItem";
            this.sallesToolStripMenuItem.Size = new System.Drawing.Size(72, 29);
            this.sallesToolStripMenuItem.Text = "Salles";
            this.sallesToolStripMenuItem.Click += new System.EventHandler(this.sallesToolStripMenuItem_Click);
            // 
            // liguesToolStripMenuItem
            // 
            this.liguesToolStripMenuItem.Name = "liguesToolStripMenuItem";
            this.liguesToolStripMenuItem.Size = new System.Drawing.Size(78, 29);
            this.liguesToolStripMenuItem.Text = "Ligues";
            this.liguesToolStripMenuItem.Click += new System.EventHandler(this.liguesToolStripMenuItem_Click);
            // 
            // réservantsToolStripMenuItem
            // 
            this.réservantsToolStripMenuItem.Name = "réservantsToolStripMenuItem";
            this.réservantsToolStripMenuItem.Size = new System.Drawing.Size(113, 29);
            this.réservantsToolStripMenuItem.Text = "Réservants";
            this.réservantsToolStripMenuItem.Click += new System.EventHandler(this.réservantsToolStripMenuItem_Click_1);
            // 
            // equipementsToolStripMenuItem
            // 
            this.equipementsToolStripMenuItem.Name = "equipementsToolStripMenuItem";
            this.equipementsToolStripMenuItem.Size = new System.Drawing.Size(133, 29);
            this.equipementsToolStripMenuItem.Text = "Equipements";
            this.equipementsToolStripMenuItem.Click += new System.EventHandler(this.equipementsToolStripMenuItem_Click);
            // 
            // réservationsToolStripMenuItem
            // 
            this.réservationsToolStripMenuItem.Name = "réservationsToolStripMenuItem";
            this.réservationsToolStripMenuItem.Size = new System.Drawing.Size(129, 29);
            this.réservationsToolStripMenuItem.Text = "Réservations";
            this.réservationsToolStripMenuItem.Click += new System.EventHandler(this.réservationsToolStripMenuItem_Click);
            // 
            // disponibilitésToolStripMenuItem
            // 
            this.disponibilitésToolStripMenuItem.Name = "disponibilitésToolStripMenuItem";
            this.disponibilitésToolStripMenuItem.Size = new System.Drawing.Size(138, 29);
            this.disponibilitésToolStripMenuItem.Text = "Disponibilités";
            this.disponibilitésToolStripMenuItem.Click += new System.EventHandler(this.disponibilitésToolStripMenuItem_Click);
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ouiToolStripMenuItem});
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(83, 29);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click_1);
            // 
            // ouiToolStripMenuItem
            // 
            this.ouiToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ouiToolStripMenuItem.Name = "ouiToolStripMenuItem";
            this.ouiToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.ouiToolStripMenuItem.Click += new System.EventHandler(this.ouiToolStripMenuItem_Click);
            // 
            // frmM2LReservationSalles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1094, 507);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmM2LReservationSalles";
            this.Text = "Maison des ligues - Réservation salles";
            this.Load += new System.EventHandler(this.frmM2LReservationSalles_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem sallesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem réservantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem liguesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem réservationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem disponibilitésToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem equipementsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ouiToolStripMenuItem;
    }
}

