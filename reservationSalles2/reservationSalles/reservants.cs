﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class frmReservants : Form
    {
        
        public frmReservants()
        {
            InitializeComponent();
        }

        private void reservants_Load(object sender, EventArgs e)
        {
          
            tbxInseeReservant.Enabled = false;
            tbxNomReservant.Enabled = false;
            tbxPrenomReservant.Enabled = false;
            tbxTelephoneReservant.Enabled = false;
            tbxMailReservant.Enabled = false;
            cbxLigueReservant.Enabled = false;
            tbxFonctionReservant.Enabled = false;


            btnEnregistrerReservant.Visible = false;
            btnAnnulerReservant.Visible = false;
            btnRechercherReservant.Enabled = true;
        }

     

        private void btnAjouterReservant_Click(object sender, EventArgs e)
        {
            tbxInseeReservant.Text = "";
            tbxNomReservant.Text = "";
            tbxPrenomReservant.Text = "";
            tbxTelephoneReservant.Text = "";
            tbxMailReservant.Text = "";
            tbxFonctionReservant.Text = "";
           
            tbxInseeReservant.Enabled = true;
            tbxNomReservant.Enabled = true;
            tbxPrenomReservant.Enabled = true;
            tbxTelephoneReservant.Enabled = true;
            tbxMailReservant.Enabled = true;
            cbxLigueReservant.Enabled = true;
            tbxFonctionReservant.Enabled = true;

            btnAjouterReservant.Visible = false;
            btnSupprimerReservant.Visible = false;
            btnModifierReservant.Enabled = false;

            btnEnregistrerReservant.Visible = true;
            btnAnnulerReservant.Visible = true;

            lbxReservants.Enabled = false;
            tbxRechercherReservant.Enabled = false;
            btnRechercherReservant.Enabled = false;
        }

        private void btnEnregistrerReservant_Click(object sender, EventArgs e)
        {
            
                 

            //A faire : vérifier saisie
            
                tbxInseeReservant.Enabled = false;
                tbxNomReservant.Enabled = false;
                tbxPrenomReservant.Enabled = false;
                tbxTelephoneReservant.Enabled = false;
                tbxMailReservant.Enabled = false;
                cbxLigueReservant.Enabled = false;
                tbxFonctionReservant.Enabled = false;

                btnAjouterReservant.Visible = true;
                btnSupprimerReservant.Visible = true;
                btnModifierReservant.Enabled = true;

                btnEnregistrerReservant.Visible = false;
                btnAnnulerReservant.Visible = false;

                lbxReservants.Enabled = true;
                tbxRechercherReservant.Enabled = true;
                btnRechercherReservant.Enabled = true;

               
           
        }

       
    }
}
