﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class frmM2LReservationSalles : Form
    {
        public frmM2LReservationSalles()
        {
            InitializeComponent();
        }


        


        private void sallesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "frmSalles")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                frmSalles fSalles = new frmSalles();
                fSalles.MdiParent = this;
                fSalles.WindowState = FormWindowState.Maximized;
                fSalles.Show();
            }

        }


      


     

        private void réservantsToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "frmReservants")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                frmReservants Freservants = new frmReservants();
                Freservants.MdiParent = this;
                Freservants.WindowState = FormWindowState.Maximized;
                Freservants.Show();

            }

        }

       


        private void liguesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "frmLigues")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                frmLigues fLigues = new frmLigues();
                fLigues.MdiParent = this;
                fLigues.WindowState = FormWindowState.Maximized;
                fLigues.Show();
             
            }

        }

        private void réservationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "frmReservations")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                frmReservations fReservations = new frmReservations();
                fReservations.MdiParent = this;
                fReservations.WindowState = FormWindowState.Maximized;
                fReservations.Show();

            }
        }

       
        private void equipementsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "frmEquipements")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                frmEquipements fEquipements = new frmEquipements();
                fEquipements.MdiParent = this;
                fEquipements.WindowState = FormWindowState.Maximized;
                fEquipements.Show();

            }
        }

        private void disponibilitésToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null && this.ActiveMdiChild.Text != "frmDisponibilites")
            {
                this.ActiveMdiChild.Close();
            }

            if (this.ActiveMdiChild == null)
            {
                frmDisponibilites fDisponibilites = new frmDisponibilites();
                fDisponibilites.MdiParent = this;
                fDisponibilites.WindowState = FormWindowState.Maximized;
                fDisponibilites.Show();

            }
        }

        private void quitterToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmM2LReservationSalles_Load(object sender, EventArgs e)
        {

        }

        private void ouiToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
