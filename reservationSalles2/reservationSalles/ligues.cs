﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class frmLigues : Form
    {
        public frmLigues()
        {
            InitializeComponent();
        }

        private void ligues_Load(object sender, EventArgs e)
        {
          
            tbxNomLigue.Enabled = false;
            tbxTelephoneLigue.Enabled = false;
            tbxMailLigue.Enabled = false;

            btnEnregistrerLigue.Visible = false;
            btnAnnulerLigue.Visible = false;

            btnRechercherLigue.Enabled = true;
        }

      

     

        private void btnAjouterLigue_Click(object sender, EventArgs e)
        {
            tbxNomLigue.Text = "";
            tbxTelephoneLigue.Text = "";
            tbxMailLigue.Text = "";

            tbxNomLigue.Enabled = true;
            tbxTelephoneLigue.Enabled = true;
            tbxMailLigue.Enabled = true;

            btnAjouterLigue.Visible = false;
            btnSupprimerLigue.Visible = false;
            btnModifierLigue.Enabled = false;

            btnEnregistrerLigue.Visible = true;
            btnAnnulerLigue.Visible = true;

            lbxLigues.Enabled = false;
            tbxRechercherLigue.Enabled = false;
            btnRechercherLigue.Enabled = false;
        }

        private void btnEnregistrerLigue_Click(object sender, EventArgs e)
        {
           

               

            //A faire : Vérifier saisie

           
              


                tbxNomLigue.Enabled = false;
                tbxTelephoneLigue.Enabled = false;
                tbxMailLigue.Enabled = false;

                btnAjouterLigue.Visible = true;
                btnSupprimerLigue.Visible = true;
                btnModifierLigue.Enabled = true;

                btnEnregistrerLigue.Visible = false;
                btnAnnulerLigue.Visible = false;

                lbxLigues.Enabled = true;
                tbxRechercherLigue.Enabled = true;
                btnRechercherLigue.Enabled = true;

               
            }

        }

       


       
    
}
