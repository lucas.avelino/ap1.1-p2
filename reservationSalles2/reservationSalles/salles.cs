﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class frmSalles : Form
    {
       

        public frmSalles()
        {
            InitializeComponent();
        }

        private void salles_Load(object sender, EventArgs e)
        {

            tbxNomSalle.Enabled = false;
            tbxTypeSalle.Enabled = false;
            tbxSurfaceSalle.Enabled = false;
            tbxCapaciteSalle.Enabled = false;

            btnEnregistrerSalle.Visible = false;
            btnAnnulerSalle.Visible = false;
        }

       

        

        private void btnModifierSalle_Click(object sender, EventArgs e)
        {
            if (tbxNomSalle.Text != "")
            {
                tbxNomSalle.Enabled = true;
                tbxTypeSalle.Enabled = true;
                tbxSurfaceSalle.Enabled = true;
                tbxCapaciteSalle.Enabled = true;

                btnAjouterSalle.Visible = false;
                btnSupprimerSalle.Visible = false;
                btnModifierSalle.Enabled = false;

                btnEnregistrerSalle.Visible = true;
                btnAnnulerSalle.Visible = true;

                lbxSalles.Enabled = false;
                tbxRechercherSalle.Enabled = false;
            }
        }

       
        private void btnAjouterSalle_Click(object sender, EventArgs e)
        {
            tbxNomSalle.Text = "";
            tbxTypeSalle.Text = "";
            tbxSurfaceSalle.Text = "";
            tbxCapaciteSalle.Text = "";

            tbxNomSalle.Enabled = true;
            tbxTypeSalle.Enabled = true;
            tbxSurfaceSalle.Enabled = true;
            tbxCapaciteSalle.Enabled = true;

            btnAjouterSalle.Visible = false;
            btnSupprimerSalle.Visible = false;
            btnModifierSalle.Enabled = false;

            btnEnregistrerSalle.Visible = true;
            btnAnnulerSalle.Visible = true;

            lbxSalles.Enabled = false;
            tbxRechercherSalle.Enabled = false;
        }

        private void btnEnregistrerSalle_Click(object sender, EventArgs e)
        {


            // A faire : vérifier saisie



            tbxNomSalle.Enabled = false;
            tbxTypeSalle.Enabled = false;
            tbxSurfaceSalle.Enabled = false;
            tbxCapaciteSalle.Enabled = false;

            btnAjouterSalle.Visible = true;
            btnSupprimerSalle.Visible = true;
            btnModifierSalle.Enabled = true;

            btnEnregistrerSalle.Visible = false;
            btnAnnulerSalle.Visible = false;

            lbxSalles.Enabled = true;
            tbxRechercherSalle.Enabled = true;
                       
        }

       
    }
}
