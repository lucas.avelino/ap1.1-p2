﻿
namespace reservationSalles2018
{
    partial class frmReservations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnSupprimerReservation = new System.Windows.Forms.Button();
            this.btnModifierReservation = new System.Windows.Forms.Button();
            this.btnAjouterReservation = new System.Windows.Forms.Button();
            this.btnSemaineSuivante = new System.Windows.Forms.Button();
            this.btnSemainePrecedente = new System.Windows.Forms.Button();
            this.tbxPlageReservation = new System.Windows.Forms.TextBox();
            this.tbxDateReservation = new System.Windows.Forms.TextBox();
            this.cbxSemaines = new System.Windows.Forms.ComboBox();
            this.dgvSemaine = new System.Windows.Forms.DataGridView();
            this.cbxReservants = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.lbxSalles = new System.Windows.Forms.ListBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.listBox3 = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSemaine)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(82, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(363, 33);
            this.label1.TabIndex = 41;
            this.label1.Text = "Gestion des réservations";
            // 
            // btnSupprimerReservation
            // 
            this.btnSupprimerReservation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSupprimerReservation.Location = new System.Drawing.Point(323, 402);
            this.btnSupprimerReservation.Name = "btnSupprimerReservation";
            this.btnSupprimerReservation.Size = new System.Drawing.Size(91, 33);
            this.btnSupprimerReservation.TabIndex = 67;
            this.btnSupprimerReservation.Text = "Supprimer";
            this.btnSupprimerReservation.UseVisualStyleBackColor = true;
            // 
            // btnModifierReservation
            // 
            this.btnModifierReservation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifierReservation.Location = new System.Drawing.Point(201, 402);
            this.btnModifierReservation.Name = "btnModifierReservation";
            this.btnModifierReservation.Size = new System.Drawing.Size(91, 33);
            this.btnModifierReservation.TabIndex = 66;
            this.btnModifierReservation.Text = "Modifier";
            this.btnModifierReservation.UseVisualStyleBackColor = true;
            // 
            // btnAjouterReservation
            // 
            this.btnAjouterReservation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterReservation.Location = new System.Drawing.Point(81, 402);
            this.btnAjouterReservation.Name = "btnAjouterReservation";
            this.btnAjouterReservation.Size = new System.Drawing.Size(91, 33);
            this.btnAjouterReservation.TabIndex = 65;
            this.btnAjouterReservation.Text = "Ajouter";
            this.btnAjouterReservation.UseVisualStyleBackColor = true;
            // 
            // btnSemaineSuivante
            // 
            this.btnSemaineSuivante.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSemaineSuivante.Location = new System.Drawing.Point(904, 126);
            this.btnSemaineSuivante.Name = "btnSemaineSuivante";
            this.btnSemaineSuivante.Size = new System.Drawing.Size(174, 39);
            this.btnSemaineSuivante.TabIndex = 64;
            this.btnSemaineSuivante.Text = "Semaine Suivante";
            this.btnSemaineSuivante.UseVisualStyleBackColor = true;
            this.btnSemaineSuivante.Click += new System.EventHandler(this.btnSemaineSuivante_Click);
            // 
            // btnSemainePrecedente
            // 
            this.btnSemainePrecedente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSemainePrecedente.Location = new System.Drawing.Point(517, 126);
            this.btnSemainePrecedente.Name = "btnSemainePrecedente";
            this.btnSemainePrecedente.Size = new System.Drawing.Size(174, 39);
            this.btnSemainePrecedente.TabIndex = 63;
            this.btnSemainePrecedente.Text = "Semaine précédente";
            this.btnSemainePrecedente.UseVisualStyleBackColor = true;
            this.btnSemainePrecedente.Click += new System.EventHandler(this.btnSemainePrecedente_Click);
            // 
            // tbxPlageReservation
            // 
            this.tbxPlageReservation.Location = new System.Drawing.Point(306, 94);
            this.tbxPlageReservation.Name = "tbxPlageReservation";
            this.tbxPlageReservation.Size = new System.Drawing.Size(179, 20);
            this.tbxPlageReservation.TabIndex = 62;
            this.tbxPlageReservation.TextChanged += new System.EventHandler(this.tbxPlageReservation_TextChanged);
            // 
            // tbxDateReservation
            // 
            this.tbxDateReservation.Location = new System.Drawing.Point(88, 96);
            this.tbxDateReservation.Name = "tbxDateReservation";
            this.tbxDateReservation.Size = new System.Drawing.Size(150, 20);
            this.tbxDateReservation.TabIndex = 61;
            // 
            // cbxSemaines
            // 
            this.cbxSemaines.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxSemaines.FormattingEnabled = true;
            this.cbxSemaines.Location = new System.Drawing.Point(714, 134);
            this.cbxSemaines.Name = "cbxSemaines";
            this.cbxSemaines.Size = new System.Drawing.Size(166, 24);
            this.cbxSemaines.TabIndex = 59;
            this.cbxSemaines.SelectedIndexChanged += new System.EventHandler(this.cbxSemaines_SelectedIndexChanged);
            // 
            // dgvSemaine
            // 
            this.dgvSemaine.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSemaine.Location = new System.Drawing.Point(513, 188);
            this.dgvSemaine.Name = "dgvSemaine";
            this.dgvSemaine.ReadOnly = true;
            this.dgvSemaine.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvSemaine.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvSemaine.Size = new System.Drawing.Size(575, 252);
            this.dgvSemaine.TabIndex = 58;
            // 
            // cbxReservants
            // 
            this.cbxReservants.FormattingEnabled = true;
            this.cbxReservants.Location = new System.Drawing.Point(215, 335);
            this.cbxReservants.Name = "cbxReservants";
            this.cbxReservants.Size = new System.Drawing.Size(211, 21);
            this.cbxReservants.TabIndex = 57;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 20);
            this.label4.TabIndex = 56;
            this.label4.Text = "Date";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(77, 333);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.TabIndex = 55;
            this.label3.Text = "Reservant";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(255, 94);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 20);
            this.label2.TabIndex = 54;
            this.label2.Text = "Fin";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 20);
            this.label6.TabIndex = 53;
            this.label6.Text = "Salle";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(260, 146);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 20);
            this.label7.TabIndex = 70;
            this.label7.Text = "Equipements";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(162, 368);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(111, 20);
            this.label8.TabIndex = 71;
            this.label8.Text = "Montant total: ";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(279, 368);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(31, 20);
            this.label9.TabIndex = 72;
            this.label9.Text = "0 €";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(731, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(163, 20);
            this.label5.TabIndex = 73;
            this.label5.Text = "Liste des réservations";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(23, 169);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(221, 21);
            this.comboBox2.TabIndex = 75;
            this.comboBox2.Text = "Type";
            // 
            // lbxSalles
            // 
            this.lbxSalles.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxSalles.FormattingEnabled = true;
            this.lbxSalles.ItemHeight = 20;
            this.lbxSalles.Location = new System.Drawing.Point(23, 196);
            this.lbxSalles.Name = "lbxSalles";
            this.lbxSalles.Size = new System.Drawing.Size(221, 84);
            this.lbxSalles.TabIndex = 74;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(114, 148);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(130, 21);
            this.comboBox3.TabIndex = 76;
            this.comboBox3.Text = "Capacité";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(369, 148);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(116, 21);
            this.comboBox1.TabIndex = 79;
            this.comboBox1.Text = "Quantité";
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(264, 169);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(221, 21);
            this.comboBox4.TabIndex = 78;
            this.comboBox4.Text = "Type";
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(264, 196);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(221, 84);
            this.listBox1.TabIndex = 77;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(284, 283);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 22);
            this.button1.TabIndex = 80;
            this.button1.Text = "+";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(438, 283);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(29, 22);
            this.button2.TabIndex = 81;
            this.button2.Text = "-";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(168, 286);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(29, 22);
            this.button3.TabIndex = 83;
            this.button3.Text = "-";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(58, 286);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(29, 22);
            this.button4.TabIndex = 82;
            this.button4.Text = "+";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(401, 283);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 20);
            this.label10.TabIndex = 85;
            this.label10.Text = "0 €";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(319, 283);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(76, 20);
            this.label11.TabIndex = 84;
            this.label11.Text = "Montant: ";
            // 
            // listBox3
            // 
            this.listBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox3.FormattingEnabled = true;
            this.listBox3.ItemHeight = 20;
            this.listBox3.Location = new System.Drawing.Point(699, 33);
            this.listBox3.Name = "listBox3";
            this.listBox3.Size = new System.Drawing.Size(221, 84);
            this.listBox3.TabIndex = 86;
            this.listBox3.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // frmReservations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 476);
            this.Controls.Add(this.listBox3);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.comboBox4);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.lbxSalles);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnSupprimerReservation);
            this.Controls.Add(this.btnModifierReservation);
            this.Controls.Add(this.btnAjouterReservation);
            this.Controls.Add(this.btnSemaineSuivante);
            this.Controls.Add(this.btnSemainePrecedente);
            this.Controls.Add(this.tbxPlageReservation);
            this.Controls.Add(this.tbxDateReservation);
            this.Controls.Add(this.cbxSemaines);
            this.Controls.Add(this.dgvSemaine);
            this.Controls.Add(this.cbxReservants);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Name = "frmReservations";
            this.Text = "reservations";
            this.Load += new System.EventHandler(this.frmReservations_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSemaine)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSupprimerReservation;
        private System.Windows.Forms.Button btnModifierReservation;
        private System.Windows.Forms.Button btnAjouterReservation;
        private System.Windows.Forms.Button btnSemaineSuivante;
        private System.Windows.Forms.Button btnSemainePrecedente;
        private System.Windows.Forms.TextBox tbxPlageReservation;
        private System.Windows.Forms.TextBox tbxDateReservation;
        private System.Windows.Forms.ComboBox cbxSemaines;
        private System.Windows.Forms.DataGridView dgvSemaine;
        private System.Windows.Forms.ComboBox cbxReservants;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ListBox lbxSalles;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ListBox listBox3;
    }
}