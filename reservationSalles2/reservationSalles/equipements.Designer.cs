﻿
namespace reservationSalles2018
{
    partial class frmEquipements
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEquipements));
            this.label1 = new System.Windows.Forms.Label();
            this.lbxSalles = new System.Windows.Forms.ListBox();
            this.btnRechercheSalle = new System.Windows.Forms.Button();
            this.tbxCapaciteSalle = new System.Windows.Forms.TextBox();
            this.tbxSurfaceSalle = new System.Windows.Forms.TextBox();
            this.tbxTypeSalle = new System.Windows.Forms.TextBox();
            this.tbxNomSalle = new System.Windows.Forms.TextBox();
            this.tbxRechercherSalle = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAjouterSalle = new System.Windows.Forms.Button();
            this.btnSupprimerSalle = new System.Windows.Forms.Button();
            this.btnModifierSalle = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(204, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(369, 33);
            this.label1.TabIndex = 41;
            this.label1.Text = "Gestion des équipements";
            // 
            // lbxSalles
            // 
            this.lbxSalles.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxSalles.FormattingEnabled = true;
            this.lbxSalles.ItemHeight = 20;
            this.lbxSalles.Location = new System.Drawing.Point(23, 134);
            this.lbxSalles.Name = "lbxSalles";
            this.lbxSalles.Size = new System.Drawing.Size(259, 304);
            this.lbxSalles.TabIndex = 42;
            this.lbxSalles.SelectedIndexChanged += new System.EventHandler(this.lbxSalles_SelectedIndexChanged);
            // 
            // btnRechercheSalle
            // 
            this.btnRechercheSalle.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnRechercheSalle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRechercheSalle.Image = ((System.Drawing.Image)(resources.GetObject("btnRechercheSalle.Image")));
            this.btnRechercheSalle.Location = new System.Drawing.Point(247, 76);
            this.btnRechercheSalle.Name = "btnRechercheSalle";
            this.btnRechercheSalle.Size = new System.Drawing.Size(35, 38);
            this.btnRechercheSalle.TabIndex = 48;
            this.btnRechercheSalle.TabStop = false;
            this.btnRechercheSalle.UseVisualStyleBackColor = true;
            // 
            // tbxCapaciteSalle
            // 
            this.tbxCapaciteSalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxCapaciteSalle.Location = new System.Drawing.Point(508, 313);
            this.tbxCapaciteSalle.Name = "tbxCapaciteSalle";
            this.tbxCapaciteSalle.Size = new System.Drawing.Size(241, 26);
            this.tbxCapaciteSalle.TabIndex = 47;
            this.tbxCapaciteSalle.TextChanged += new System.EventHandler(this.tbxCapaciteSalle_TextChanged);
            // 
            // tbxSurfaceSalle
            // 
            this.tbxSurfaceSalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxSurfaceSalle.Location = new System.Drawing.Point(508, 257);
            this.tbxSurfaceSalle.Name = "tbxSurfaceSalle";
            this.tbxSurfaceSalle.Size = new System.Drawing.Size(241, 26);
            this.tbxSurfaceSalle.TabIndex = 46;
            this.tbxSurfaceSalle.TextChanged += new System.EventHandler(this.tbxSurfaceSalle_TextChanged);
            // 
            // tbxTypeSalle
            // 
            this.tbxTypeSalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxTypeSalle.Location = new System.Drawing.Point(508, 201);
            this.tbxTypeSalle.Name = "tbxTypeSalle";
            this.tbxTypeSalle.Size = new System.Drawing.Size(241, 26);
            this.tbxTypeSalle.TabIndex = 45;
            this.tbxTypeSalle.TextChanged += new System.EventHandler(this.tbxTypeSalle_TextChanged);
            // 
            // tbxNomSalle
            // 
            this.tbxNomSalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNomSalle.Location = new System.Drawing.Point(508, 145);
            this.tbxNomSalle.Name = "tbxNomSalle";
            this.tbxNomSalle.Size = new System.Drawing.Size(241, 26);
            this.tbxNomSalle.TabIndex = 44;
            this.tbxNomSalle.TextChanged += new System.EventHandler(this.tbxNomSalle_TextChanged);
            // 
            // tbxRechercherSalle
            // 
            this.tbxRechercherSalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRechercherSalle.Location = new System.Drawing.Point(23, 81);
            this.tbxRechercherSalle.Name = "tbxRechercherSalle";
            this.tbxRechercherSalle.Size = new System.Drawing.Size(198, 26);
            this.tbxRechercherSalle.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(354, 316);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 20);
            this.label5.TabIndex = 52;
            this.label5.Text = "Tarif";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(354, 260);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(70, 20);
            this.label4.TabIndex = 51;
            this.label4.Text = "Quantité";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(354, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 20);
            this.label3.TabIndex = 50;
            this.label3.Text = "Nom";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(354, 148);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 49;
            this.label2.Text = "Numéro";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // btnAjouterSalle
            // 
            this.btnAjouterSalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterSalle.Location = new System.Drawing.Point(650, 377);
            this.btnAjouterSalle.Name = "btnAjouterSalle";
            this.btnAjouterSalle.Size = new System.Drawing.Size(116, 42);
            this.btnAjouterSalle.TabIndex = 55;
            this.btnAjouterSalle.Text = "Ajouter";
            this.btnAjouterSalle.UseVisualStyleBackColor = true;
            // 
            // btnSupprimerSalle
            // 
            this.btnSupprimerSalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSupprimerSalle.Location = new System.Drawing.Point(499, 377);
            this.btnSupprimerSalle.Name = "btnSupprimerSalle";
            this.btnSupprimerSalle.Size = new System.Drawing.Size(116, 42);
            this.btnSupprimerSalle.TabIndex = 54;
            this.btnSupprimerSalle.Text = "Supprimer";
            this.btnSupprimerSalle.UseVisualStyleBackColor = true;
            // 
            // btnModifierSalle
            // 
            this.btnModifierSalle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifierSalle.Location = new System.Drawing.Point(345, 377);
            this.btnModifierSalle.Name = "btnModifierSalle";
            this.btnModifierSalle.Size = new System.Drawing.Size(116, 42);
            this.btnModifierSalle.TabIndex = 53;
            this.btnModifierSalle.Text = "Modifier";
            this.btnModifierSalle.UseVisualStyleBackColor = true;
            // 
            // frmEquipements
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnAjouterSalle);
            this.Controls.Add(this.btnSupprimerSalle);
            this.Controls.Add(this.btnModifierSalle);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnRechercheSalle);
            this.Controls.Add(this.tbxCapaciteSalle);
            this.Controls.Add(this.tbxSurfaceSalle);
            this.Controls.Add(this.tbxTypeSalle);
            this.Controls.Add(this.tbxNomSalle);
            this.Controls.Add(this.tbxRechercherSalle);
            this.Controls.Add(this.lbxSalles);
            this.Controls.Add(this.label1);
            this.Name = "frmEquipements";
            this.Text = "equipements";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lbxSalles;
        private System.Windows.Forms.Button btnRechercheSalle;
        private System.Windows.Forms.TextBox tbxCapaciteSalle;
        private System.Windows.Forms.TextBox tbxSurfaceSalle;
        private System.Windows.Forms.TextBox tbxTypeSalle;
        private System.Windows.Forms.TextBox tbxNomSalle;
        private System.Windows.Forms.TextBox tbxRechercherSalle;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAjouterSalle;
        private System.Windows.Forms.Button btnSupprimerSalle;
        private System.Windows.Forms.Button btnModifierSalle;
    }
}