﻿namespace reservationSalles2018
{
    partial class frmReservants
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReservants));
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbxMailReservant = new System.Windows.Forms.TextBox();
            this.tbxTelephoneReservant = new System.Windows.Forms.TextBox();
            this.tbxNomReservant = new System.Windows.Forms.TextBox();
            this.btnAjouterReservant = new System.Windows.Forms.Button();
            this.btnEnregistrerReservant = new System.Windows.Forms.Button();
            this.btnSupprimerReservant = new System.Windows.Forms.Button();
            this.btnAnnulerReservant = new System.Windows.Forms.Button();
            this.btnModifierReservant = new System.Windows.Forms.Button();
            this.btnRechercherReservant = new System.Windows.Forms.Button();
            this.tbxRechercherReservant = new System.Windows.Forms.TextBox();
            this.lbxReservants = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbxPrenomReservant = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbxInseeReservant = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbxLigueReservant = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox15 = new System.Windows.Forms.ComboBox();
            this.tbxFonctionReservant = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(532, 250);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 20);
            this.label4.TabIndex = 43;
            this.label4.Text = "Mail";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(527, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 20);
            this.label3.TabIndex = 42;
            this.label3.Text = "Téléphone";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(527, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 20);
            this.label2.TabIndex = 41;
            this.label2.Text = "Nom";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(550, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(338, 33);
            this.label1.TabIndex = 40;
            this.label1.Text = "Gestion des réservants";
            // 
            // tbxMailReservant
            // 
            this.tbxMailReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxMailReservant.Location = new System.Drawing.Point(665, 244);
            this.tbxMailReservant.Name = "tbxMailReservant";
            this.tbxMailReservant.Size = new System.Drawing.Size(241, 26);
            this.tbxMailReservant.TabIndex = 39;
            // 
            // tbxTelephoneReservant
            // 
            this.tbxTelephoneReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxTelephoneReservant.Location = new System.Drawing.Point(665, 198);
            this.tbxTelephoneReservant.Name = "tbxTelephoneReservant";
            this.tbxTelephoneReservant.Size = new System.Drawing.Size(241, 26);
            this.tbxTelephoneReservant.TabIndex = 38;
            // 
            // tbxNomReservant
            // 
            this.tbxNomReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxNomReservant.Location = new System.Drawing.Point(665, 106);
            this.tbxNomReservant.Name = "tbxNomReservant";
            this.tbxNomReservant.Size = new System.Drawing.Size(241, 26);
            this.tbxNomReservant.TabIndex = 37;
            // 
            // btnAjouterReservant
            // 
            this.btnAjouterReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAjouterReservant.Location = new System.Drawing.Point(850, 389);
            this.btnAjouterReservant.Name = "btnAjouterReservant";
            this.btnAjouterReservant.Size = new System.Drawing.Size(116, 42);
            this.btnAjouterReservant.TabIndex = 36;
            this.btnAjouterReservant.Text = "Ajouter";
            this.btnAjouterReservant.UseVisualStyleBackColor = true;
            this.btnAjouterReservant.Click += new System.EventHandler(this.btnAjouterReservant_Click);
            // 
            // btnEnregistrerReservant
            // 
            this.btnEnregistrerReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnregistrerReservant.Location = new System.Drawing.Point(850, 389);
            this.btnEnregistrerReservant.Name = "btnEnregistrerReservant";
            this.btnEnregistrerReservant.Size = new System.Drawing.Size(116, 42);
            this.btnEnregistrerReservant.TabIndex = 35;
            this.btnEnregistrerReservant.Text = "Enregistrer";
            this.btnEnregistrerReservant.UseVisualStyleBackColor = true;
            this.btnEnregistrerReservant.Click += new System.EventHandler(this.btnEnregistrerReservant_Click);
            // 
            // btnSupprimerReservant
            // 
            this.btnSupprimerReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSupprimerReservant.Location = new System.Drawing.Point(699, 389);
            this.btnSupprimerReservant.Name = "btnSupprimerReservant";
            this.btnSupprimerReservant.Size = new System.Drawing.Size(116, 42);
            this.btnSupprimerReservant.TabIndex = 34;
            this.btnSupprimerReservant.Text = "Supprimer";
            this.btnSupprimerReservant.UseVisualStyleBackColor = true;
            // 
            // btnAnnulerReservant
            // 
            this.btnAnnulerReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnulerReservant.Location = new System.Drawing.Point(699, 389);
            this.btnAnnulerReservant.Name = "btnAnnulerReservant";
            this.btnAnnulerReservant.Size = new System.Drawing.Size(116, 42);
            this.btnAnnulerReservant.TabIndex = 33;
            this.btnAnnulerReservant.Text = "Annuler";
            this.btnAnnulerReservant.UseVisualStyleBackColor = true;
            // 
            // btnModifierReservant
            // 
            this.btnModifierReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModifierReservant.Location = new System.Drawing.Point(545, 389);
            this.btnModifierReservant.Name = "btnModifierReservant";
            this.btnModifierReservant.Size = new System.Drawing.Size(116, 42);
            this.btnModifierReservant.TabIndex = 32;
            this.btnModifierReservant.Text = "Modifier";
            this.btnModifierReservant.UseVisualStyleBackColor = true;
            // 
            // btnRechercherReservant
            // 
            this.btnRechercherReservant.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnRechercherReservant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnRechercherReservant.Image = ((System.Drawing.Image)(resources.GetObject("btnRechercherReservant.Image")));
            this.btnRechercherReservant.Location = new System.Drawing.Point(357, 30);
            this.btnRechercherReservant.Name = "btnRechercherReservant";
            this.btnRechercherReservant.Size = new System.Drawing.Size(35, 38);
            this.btnRechercherReservant.TabIndex = 31;
            this.btnRechercherReservant.TabStop = false;
            this.btnRechercherReservant.UseVisualStyleBackColor = true;
            // 
            // tbxRechercherReservant
            // 
            this.tbxRechercherReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRechercherReservant.Location = new System.Drawing.Point(92, 35);
            this.tbxRechercherReservant.Name = "tbxRechercherReservant";
            this.tbxRechercherReservant.Size = new System.Drawing.Size(244, 26);
            this.tbxRechercherReservant.TabIndex = 30;
            // 
            // lbxReservants
            // 
            this.lbxReservants.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbxReservants.FormattingEnabled = true;
            this.lbxReservants.ItemHeight = 20;
            this.lbxReservants.Location = new System.Drawing.Point(92, 90);
            this.lbxReservants.Name = "lbxReservants";
            this.lbxReservants.Size = new System.Drawing.Size(300, 324);
            this.lbxReservants.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(527, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 20);
            this.label5.TabIndex = 45;
            this.label5.Text = "Prénom";
            // 
            // tbxPrenomReservant
            // 
            this.tbxPrenomReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxPrenomReservant.Location = new System.Drawing.Point(665, 152);
            this.tbxPrenomReservant.Name = "tbxPrenomReservant";
            this.tbxPrenomReservant.Size = new System.Drawing.Size(241, 26);
            this.tbxPrenomReservant.TabIndex = 44;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(527, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 20);
            this.label6.TabIndex = 47;
            this.label6.Text = "Numéro Insee";
            // 
            // tbxInseeReservant
            // 
            this.tbxInseeReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxInseeReservant.Location = new System.Drawing.Point(665, 60);
            this.tbxInseeReservant.Name = "tbxInseeReservant";
            this.tbxInseeReservant.Size = new System.Drawing.Size(241, 26);
            this.tbxInseeReservant.TabIndex = 46;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(532, 294);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 20);
            this.label7.TabIndex = 49;
            this.label7.Text = "Ligue";
            // 
            // cbxLigueReservant
            // 
            this.cbxLigueReservant.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxLigueReservant.FormattingEnabled = true;
            this.cbxLigueReservant.Location = new System.Drawing.Point(665, 291);
            this.cbxLigueReservant.Name = "cbxLigueReservant";
            this.cbxLigueReservant.Size = new System.Drawing.Size(241, 28);
            this.cbxLigueReservant.TabIndex = 50;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(532, 344);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 20);
            this.label8.TabIndex = 52;
            this.label8.Text = "Fonction";
            // 
            // comboBox15
            // 
            this.comboBox15.FormattingEnabled = true;
            this.comboBox15.Location = new System.Drawing.Point(92, 68);
            this.comboBox15.Name = "comboBox15";
            this.comboBox15.Size = new System.Drawing.Size(198, 21);
            this.comboBox15.TabIndex = 53;
            this.comboBox15.Text = "Fonction";
            // 
            // tbxFonctionReservant
            // 
            this.tbxFonctionReservant.FormattingEnabled = true;
            this.tbxFonctionReservant.Location = new System.Drawing.Point(665, 346);
            this.tbxFonctionReservant.Name = "tbxFonctionReservant";
            this.tbxFonctionReservant.Size = new System.Drawing.Size(241, 21);
            this.tbxFonctionReservant.TabIndex = 54;
            this.tbxFonctionReservant.Text = "Fonction";
            // 
            // frmReservants
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 453);
            this.Controls.Add(this.tbxFonctionReservant);
            this.Controls.Add(this.comboBox15);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.cbxLigueReservant);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbxInseeReservant);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbxPrenomReservant);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbxMailReservant);
            this.Controls.Add(this.tbxTelephoneReservant);
            this.Controls.Add(this.tbxNomReservant);
            this.Controls.Add(this.btnAjouterReservant);
            this.Controls.Add(this.btnEnregistrerReservant);
            this.Controls.Add(this.btnSupprimerReservant);
            this.Controls.Add(this.btnAnnulerReservant);
            this.Controls.Add(this.btnModifierReservant);
            this.Controls.Add(this.btnRechercherReservant);
            this.Controls.Add(this.tbxRechercherReservant);
            this.Controls.Add(this.lbxReservants);
            this.Name = "frmReservants";
            this.Text = "reservants";
            this.Load += new System.EventHandler(this.reservants_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbxMailReservant;
        private System.Windows.Forms.TextBox tbxTelephoneReservant;
        private System.Windows.Forms.TextBox tbxNomReservant;
        private System.Windows.Forms.Button btnAjouterReservant;
        private System.Windows.Forms.Button btnEnregistrerReservant;
        private System.Windows.Forms.Button btnSupprimerReservant;
        private System.Windows.Forms.Button btnAnnulerReservant;
        private System.Windows.Forms.Button btnModifierReservant;
        private System.Windows.Forms.Button btnRechercherReservant;
        private System.Windows.Forms.TextBox tbxRechercherReservant;
        private System.Windows.Forms.ListBox lbxReservants;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbxPrenomReservant;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbxInseeReservant;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbxLigueReservant;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox15;
        private System.Windows.Forms.ComboBox tbxFonctionReservant;
    }
}